package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestPoints(t *testing.T) {
	p1 := Point{7, 5}
	p2 := Point{8, 11}

	p1.Add(Vec{-4, -1})
	tools.Test(p1 == Point{3, 4}, "invalid addition of vector", t)

	tools.Test(p2.Distance(p1) == Vec{-5, -7}, "invalid distance calculation", t)

	p1.Sub(Vec{-4, -1})
	tools.Test(p1 == Point{7, 5}, "invalid subtraction of vector", t)

	p1.Scale(2)
	tools.Test(p1 == Point{14, 10}, "invalid scaling of point", t)

	p1.MoveTo(p2)
	p1.MoveRel(Vec{1, 1})
	tools.Test(p1 == Point{9, 12}, "invalid movement of point", t)

}
