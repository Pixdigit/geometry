package geometry

import (
	"math"

	"github.com/pkg/errors"
)

type Polygon struct {
	Location
	Edges []Vec
}

func NewPolygon(pts []Point) (Polygon, error) {
	switch len(pts) {
	case 0:
		return Polygon{}, errors.New("Can not create Polygon without points")
	case 1:
		return Polygon{NewLocation(pts[0]), make([]Vec, 0)}, nil
	default:
		vecs := make([]Vec, len(pts))
		lastPt := pts[0]
		for _, pt := range pts[1:] {
			vecs = append(vecs, lastPt.Distance(pt))
			lastPt = pt
		}
		vecs = append(vecs, lastPt.Distance(pts[0]))
		return Polygon{
			NewLocation(pts[0]),
			vecs,
		}, nil
	}
}

func (p Polygon) Equals(p2 Polygon) bool {
	if p.Ref() != p2.Ref() {
		return false
	}
	if len(p.Edges) != len(p2.Edges) {
		return false
	}
	for i := range p.Edges {
		if p.Edges[i] != p2.Edges[i] {
			return false
		}
	}
	return true
}

func (p Polygon) Area() (area Scalar) {
	// Using a variant of the shoelace formula
	currentP := Point{0, 0}
	lastP := currentP
	for _, v := range p.Edges {
		currentP.Add(v)
		area += (lastP.X*currentP.Y - currentP.X*lastP.Y)
		lastP = currentP
	}
	area /= 2
	return area
}

func (p Polygon) Circumference() (length Scalar) {
	for _, v := range p.Edges {
		length += v.Len()
	}
	return length
}

func (p Polygon) SanityCheck() (success bool) {
	success = false
	endPoint := Point{0, 0}
	for _, v := range p.Edges {
		endPoint.Add(v)
	}
	// The last edge should point back to the location
	return endPoint.Equals(Point{0, 0})
}

func (p *Polygon) Scale(factor Scalar) {
	//Translate so that the anchor is actually fix
	offset := p.Offset
	offset.Scale(factor)
	offset.Sub(p.Offset)
	p.Offset = offset

	for i := range p.Edges {
		p.Edges[i].Scale(factor)
	}
}

func (p *Polygon) ScaleArea(factor Scalar) {
	factor = Scalar(math.Sqrt(float64(factor)))
	p.Scale(factor)
}

func (p Polygon) Points() []Point {
	points := make([]Point, len(p.Edges))
	currPoint := p.Ref()
	points[0] = currPoint
	for i, v := range p.Edges[:len(p.Edges)-1] {
		currPoint.Add(v)
		points[i+1] = currPoint
	}
	return points
}

func (p Polygon) Lines() []Line {
	lines := make([]Line, len(p.Edges))
	startPoint := p.Ref()
	for i, v := range p.Edges[:len(p.Edges)] {
		endPoint := startPoint
		endPoint.Add(v)
		lines[i] = NewLine(startPoint, endPoint)
		startPoint = endPoint
	}
	return lines
}
