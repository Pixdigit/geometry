package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestLocationCreation(t *testing.T) {
	l := &Location{
		Point{0, 0},
		Vec{2, 1},
	}

	t.Log(l.Pos(), " != ", Point{2, 1})
	tools.Test(l.Pos() == Point{2, 1}, "position not where it is supposed to be", t)
}

func TestLocationMovement(t *testing.T) {
	l := &Location{
		Point{0, 0},
		Vec{2, 1},
	}

	l.MoveRel(Vec{3, 7})
	t.Log(l.Pos(), " != ", Point{5, 8})
	tools.Test(l.Pos() == Point{5, 8}, "relative movement not correct", t)

	l.Offset = Vec{3, 2}
	l.MoveTo(Point{2, 2})
	t.Log(l.Pos(), " != ", Point{2, 2})
	tools.Test(l.Pos() == Point{2, 2}, "absolute movement not correct", t)
	t.Log(l.Ref(), " != ", Point{-1, 0})
	tools.Test(l.Ref() == Point{-1, 0}, "absolute movement not correct", t)
}
