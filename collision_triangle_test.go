package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestTriangleCollision(t *testing.T) {
	t1 := Triangle{Point{1, 1}, Point{2, 2}, Point{3, 1}}
	t2 := Triangle{Point{0, 3}, Point{2, 0}, Point{4, 3}}

	collision, err := Collision(t1, t2)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "triangles did not collide when they should", t)

	t2.MoveTo(Point{2, 3})
	collision, err = Collision(t1, t2)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(!collision, "triangles collided when they should not", t)
}
