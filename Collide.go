package geometry

import (
	"github.com/pkg/errors"
)

type PolygonalCollider interface {
	Points() []Point
	Lines() []Line
}

type VolumetricCollider interface {
	Contains(PolygonalCollider) bool
	Intersects(PolygonalCollider) bool
}

type Collideable interface {
	CollidesWith(interface{}) (bool, error)
}

func Collision(geoObj1, geoObj2 interface{}) (bool, error) {
	switch obj1 := geoObj1.(type) {
	case Collideable:
		return obj1.CollidesWith(geoObj2)
	case VolumetricCollider:
		return volumetricCollision(obj1, geoObj2)
	case PolygonalCollider:
		return polygonCollision(obj1, geoObj2)
	default:
		return false, errors.Wrap(NewUnknownShapeError(geoObj2), "invlid type in collision")
	}
}

func volumetricCollision(obj1 VolumetricCollider, geoObj2 interface{}) (bool, error) {
	switch obj2 := geoObj2.(type) {
	case Collideable:
		return obj2.CollidesWith(obj1)
	case PolygonalCollider:
		return VolumetricPolygonCollision(obj1, obj2), nil
	default:
		return false, errors.Wrap(NewUnknownShapeError(geoObj2), "voluemtric collision failed")
	}
}

func polygonCollision(obj1 PolygonalCollider, geoObj2 interface{}) (bool, error) {
	switch obj2 := geoObj2.(type) {
	case Collideable:
		return obj2.CollidesWith(obj1)
	case VolumetricCollider:
		return VolumetricPolygonCollision(obj2, obj1), nil
	case PolygonalCollider:
		return PolygonPolygonCollision(obj1, obj2), nil
	default:
		return false, errors.Wrap(NewUnknownShapeError(geoObj2), "polygonal collision failed")
	}
}
func VolumetricPolygonCollision(obj1 VolumetricCollider, obj2 PolygonalCollider) bool {
	if obj1.Intersects(obj2) {
		return true
	}
	for _, pt := range obj2.Points() {
		if obj1.Contains(pt) {
			return true
		}
	}
	return false
}

func PolygonPolygonCollision(obj1, obj2 PolygonalCollider) bool {
	if Contains(obj1, obj2) || Contains(obj2, obj1) {
		return true
	}

	//Check if any two lines collide
	for _, l1 := range obj1.Lines() {
		for _, l2 := range obj2.Lines() {
			aStart := l1.Ref()
			aEnd := l1.End
			bStart := l2.Ref()
			bEnd := l2.End

			denom := ((aEnd.X - aStart.X) * (bEnd.Y - bStart.Y)) - ((aEnd.Y - aStart.Y) * (bEnd.X - bStart.X))
			numerator1 := ((aStart.Y - bStart.Y) * (bEnd.X - bStart.X)) - ((aStart.X - bStart.X) * (bEnd.Y - bStart.Y))
			numerator2 := ((aStart.Y - bStart.Y) * (aEnd.X - aStart.X)) - ((aStart.X - bStart.X) * (aEnd.Y - aStart.Y))

			r := numerator1 / denom
			s := numerator2 / denom

			collision := (0 <= r && r <= 1) && (0 <= s && s <= 1)
			if collision {
				return true
			}
		}
	}

	return false
}

func Contains(p1, p2 PolygonalCollider) bool {
	isContained := true
check:
	for _, pt2 := range p2.Points() {

		if WindingNumber(p1, pt2)%2 == 0 {
			//Point is not within when WindingNumber is even
			//Check if point is instead on a line
			for _, l := range p1.Lines() {
				vecLoc := Vec{
					pt2.X - l.ref.X,
					pt2.Y - l.ref.Y,
				}
				vecLine := l.Vec()
				crossP := vecLoc.X*vecLine.Y - vecLoc.Y*vecLine.X

				// If cross product is unequal to 0 that means point lays on
				// the inifinite line going trough the monogon
				if crossP == 0 {
					// Check if point is wihin line segment
					if abs(vecLine.X) >= abs(vecLine.Y) {
						if vecLine.X > 0 {
							isContained = l.ref.X <= pt2.X && pt2.X <= l.End.X
						} else {
							isContained = l.End.X <= pt2.X && pt2.X <= l.ref.X
						}
					} else {
						if vecLine.Y > 0 {
							isContained = l.ref.Y <= pt2.Y && pt2.Y <= l.End.Y
						} else {
							isContained = l.End.Y <= pt2.Y && pt2.Y <= l.ref.Y
						}
					}
				} else {
					isContained = false
				}
				if isContained {
					//Skip point check, because point is already contained
					continue check
				}
			}
			//lines may not exist so also check points
			for _, pt1 := range p1.Points() {
				isContained = pt1 == pt2
				if isContained {
					continue check
				}
			}
		}
	}

	return isContained
}

func WindingNumber(p PolygonalCollider, pt Point) int {
	wn := 0

	isLeft := func(pt Point, l Line) Scalar {
		start := l.Ref()
		end := l.End
		return ((end.X-start.X)*(pt.Y-start.Y) - (pt.X-start.X)*(end.Y-start.Y))
	}

	for _, edge := range p.Lines() {
		start := edge.Ref()
		end := edge.End
		if start.Y <= pt.Y {
			if end.Y > pt.Y {
				if isLeft(pt, edge) > 0 {
					wn++
				}
			}
		} else {
			if end.Y <= pt.Y {
				if isLeft(pt, edge) < 0 {
					wn--
				}
			}
		}
	}
	return wn
}

// 	case Point - Triangle:
// 		//Uses area calculation if point is in triangle as described here:
// 		//https://www.gamedev.net/forums/topic/295943-is-this-a-better-point-in-triangle-test-2d/
//
// 		a := obj.Area()
// 		p1, p2, p3 := obj.Points()
//
// 		t1 := NewTriangle(ref, p2, p3)
// 		t2 := NewTriangle(ref, p1, p3)
// 		t3 := NewTriangle(ref, p1, p2)
//
// 		//If combined area is smaller than triangle area then point is inside
// 		collision = (t1.Area()+t2.Area()+t3.Area() <= a)
