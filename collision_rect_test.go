package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestRectCollision(t *testing.T) {

	r1 := NewRect(Point{1, 1}, 2, 2)
	r2 := NewRect(Point{2, 2}, 1, 1)

	collision, err := Collision(r1, r2)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "rectangles did not collide when they should", t)

	r2.MoveTo(Point{4, 4})
	collision, err = Collision(r1, r2)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(!collision, "rectanlges collided when they should not", t)

	tr := Triangle{Point{0, 0}, Point{2, 0}, Point{0, 2}}
	collision, err = Collision(r1, tr)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "rectangle did not collide with trianlge", t)

	tr = Triangle{Point{-2, -2}, Point{-2, 0}, Point{0, -2}}
	collision, err = Collision(r1, tr)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(!collision, "rectangle collided with trianlge when it should not", t)

}
