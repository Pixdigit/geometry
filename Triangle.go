package geometry

import "math"

type Triangle struct {
	P1, P2, P3 Point
}

func (t Triangle) Area() Scalar {
	//See https://en.wikipedia.org/wiki/Triangle#Using_coordinates
	return ((t.P1.X-t.P3.X)*(t.P2.Y-t.P1.Y) - (t.P1.X-t.P2.X)*(t.P3.Y-t.P1.Y)) / 2
}

func (t Triangle) Circumference() Scalar {
	return (t.P1.Distance(t.P2).Len() + t.P2.Distance(t.P3).Len() + t.P3.Distance(t.P1).Len())
}

func (t Triangle) Equals(t2 Triangle) bool {
	return (t.P1 == t2.P1 && t.P2 == t2.P2 && t.P3 == t2.P3)
}

func (t Triangle) Lines() []Line {
	return []Line{
		NewLine(t.P1, t.P2),
		NewLine(t.P2, t.P3),
		NewLine(t.P3, t.P1),
	}
}

func (t *Triangle) MoveTo(pt Point) {
	t.MoveRel(t.P1.Distance(pt))
}

func (t *Triangle) MoveRel(move Vec) {
	t.P1.Add(move)
	t.P2.Add(move)
	t.P3.Add(move)
}

func (t Triangle) Points() []Point {
	return []Point{t.P1, t.P2, t.P3}
}

func (t *Triangle) Scale(factor Scalar) {
	side1 := t.P1.Distance(t.P2)
	side1.Scale(factor)
	t.P2 = t.P1
	t.P2.Add(side1)

	side2 := t.P1.Distance(t.P3)
	side2.Scale(factor)
	t.P3 = t.P1
	t.P3.Add(side2)
}

func (t *Triangle) ScaleArea(factor Scalar) {
	t.Scale(Scalar(math.Sqrt(float64(factor))))
}

//TODO: Implement improvements in collision detection
