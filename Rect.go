package geometry

import (
	"math"

	"github.com/pkg/errors"
)

// Rects are primarily used to improve performance and readability
type Rect struct {
	*Location
	W, H Scalar
}

func NewRect(p Point, w, h Scalar) Rect {
	p.Sub(Vec{w / 2, h / 2})
	return Rect{&Location{p, Vec{w / 2, h / 2}}, w, h}
}

func (r Rect) Area() Scalar {
	return r.H * r.W
}

func (r Rect) Circumference() Scalar {
	return 2 * (r.H + r.W)
}

func (r Rect) CollidesWith(geoObj2 interface{}) (bool, error) {

	switch obj2 := geoObj2.(type) {
	case Rect:
		r1Top, r1Left := r.ref.X, r.ref.Y
		r1Bottom, r1Right := r1Top+r.H, r1Left+r.W
		r2Top, r2Left := obj2.ref.X, obj2.ref.Y
		r2Bottom, r2Right := r2Top+obj2.H, r2Left+obj2.W
		//if any condition is true, then no collision can be happening
		return !(r1Left >= r2Right ||
			r1Right <= r2Left ||
			r1Top >= r2Bottom ||
			r1Bottom <= r2Top), nil

	case PolygonalCollider:
		return PolygonPolygonCollision(r, obj2), nil
	case VolumetricCollider:
		return VolumetricPolygonCollision(obj2, r), nil
	default:
		return false, errors.Wrap(NewUnknownShapeError(obj2), "Point collision: ")
	}
}

func (r Rect) Equals(r2 Rect) bool {
	return r.Location.ref == r2.Location.ref && r.W == r2.W && r.H == r2.H
}

func (r Rect) Lines() []Line {
	lines := make([]Line, 4, 4)
	pts := r.Points()

	for i := 0; i < 4; i++ {
		lines[i] = NewLine(pts[i], pts[(i+1)%4])
	}

	return lines
}

func (r Rect) Points() []Point {
	points := make([]Point, 4, 4)
	points[0] = r.Ref()

	points[1] = r.Ref()
	points[1].Add(Vec{r.W, 0})

	points[2] = r.Ref()
	points[2].Add(Vec{r.W, r.H})

	points[3] = r.Ref()
	points[3].Add(Vec{0, r.H})
	return points
}

func (r *Rect) Scale(factorX, factorY Scalar) {
	anchor := r.Pos()
	r.W *= factorX
	r.H *= factorY
	r.Offset.X = r.W / 2
	r.Offset.Y = r.H / 2
	r.MoveTo(anchor)
}

func (r *Rect) ScaleArea(factor Scalar) {
	edgeFactor := Scalar(math.Sqrt(float64(factor)))
	r.Scale(edgeFactor, edgeFactor)
}

//SetSize keeps offset proportional
func (r *Rect) SetSize(w, h Scalar) {
	r.Scale(w/r.W, h/r.H)
}

//TODO: Implement improvements in collision detection
