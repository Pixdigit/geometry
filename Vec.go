package geometry

import "math"

type Vec Point

func (v Vec) ToPoint(ref Point) *Point {
	ref.Add(v)
	return &ref
}

func (v *Vec) Add(v2 Vec) {
	v.X += v2.X
	v.Y += v2.Y
}

func (v *Vec) Sub(v2 Vec) {
	v2.Scale(-1)
	v.Add(v2)
}

func (v *Vec) Scale(s Scalar) {
	v.X *= s
	v.Y *= s
}

func (v Vec) LenSqr() Scalar {
	//pythagoras
	return square(v.X) + square(v.Y)
}

func (v Vec) Len() Scalar {
	//pythagoras
	return Scalar(math.Sqrt(float64(v.LenSqr())))
}
