package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestTriangleSpecs(t *testing.T) {
	tr := Triangle{Point{0, 0}, Point{1, 0}, Point{0, 1}}

	tools.Test(abs(tr.Area()-0.5) < tolerance, "inacurate area calculation", t)
	tools.Test(abs(tr.Circumference()-3.414213562373095) < tolerance, "inacurate circumference calculation", t)
	tr.ScaleArea(2)
	tools.Test(abs(tr.Area()-1) < tolerance, "inacurate area calculation", t)
	tools.Test(abs(tr.Circumference()-4.82842712474619) < tolerance, "inacurate circumference calculation", t)
}

func TestTriangleMovement(t *testing.T) {
	tr1 := Triangle{Point{0, 0}, Point{0, 1}, Point{1, 0}}
	tr2 := Triangle{Point{3, 3}, Point{3, 4}, Point{4, 3}}
	tr2.MoveTo(Point{-1, -1})
	tr2.MoveRel(Vec{1, 1})
	tools.Test(tr1.Equals(tr2), "triangles do not equal when they should", t)
}
