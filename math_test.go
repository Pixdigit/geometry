package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestComparators(t *testing.T) {
	a := Scalar(1)
	b := Scalar(4)
	c := Scalar(-1)

	tools.Test(max(max(a, b), c) == b, "max did not return biggest value", t)
	tools.Test(min(min(a, b), c) == c, "min did not return smallest value", t)
	tools.Test(abs(-3) == 3, "abs did not return absolute value", t)
}
