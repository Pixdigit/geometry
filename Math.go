package geometry

var tolerance Scalar = 0.0000000001

func min(a, b Scalar) Scalar {
	if a < b {
		return a
	} else {
		return b
	}
}
func max(a, b Scalar) Scalar {
	if a > b {
		return a
	} else {
		return b
	}
}

func abs(a Scalar) Scalar {
	if a < 0 {
		return -1 * a
	} else {
		return a
	}

}

func square(a Scalar) Scalar {
	return a * a
}
