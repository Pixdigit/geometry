package geometry

//Location allows for Positioning relative to the objects defining features
//(e.g. the center of a circle)
type Location struct {
	ref    Point //object's reference position
	Offset Vec   //Points from position towards new anchor
}

func NewLocation(position Point) Location {
	return Location{
		position,
		Vec{0, 0},
	}
}

//Get the reference point == the real position
func (l Location) Ref() Point {
	return l.ref
}

func (l Location) Pos() Point {
	p := l.ref
	p.Add(l.Offset)
	return p
}

func (l *Location) MoveTo(p2 Point) {
	//move p2 according to offset
	p2.Sub(l.Offset)
	l.ref.MoveTo(p2)
}

func (l *Location) MoveRel(v Vec) {
	l.ref.MoveRel(v)
}
