package geometry

import (
	"github.com/pkg/errors"
)

type Scalar float64

type Point struct {
	X, Y Scalar
}

func (pt Point) Equals(p2 Point) bool {
	return pt == p2
}

func (pt *Point) Add(v Vec) {
	pt.X += v.X
	pt.Y += v.Y
}
func (pt *Point) Sub(v Vec) {
	v.Scale(-1)
	pt.Add(v)
}
func (pt *Point) Scale(s Scalar) {
	pt.X *= s
	pt.Y *= s
}

func (pt *Point) MoveRel(v Vec) {
	pt.Add(v)
}
func (pt *Point) MoveTo(p2 Point) {
	pt.X = p2.X
	pt.Y = p2.Y
}

func (pt Point) Distance(p2 Point) Vec {
	//How to get from pt to p2
	return Vec{p2.X - pt.X, p2.Y - pt.Y}
}

func (pt Point) Lines() []Line {
	return []Line{}
}
func (pt Point) Points() []Point {
	return []Point{pt}
}

func (pt Point) CollidesWith(geoObj2 interface{}) (bool, error) {

	switch obj2 := geoObj2.(type) {
	case Rect:
		points := obj2.Points()

		//While direction is not defined, points by one point apart will always
		//be opposite
		bottom := max(points[0].Y, points[2].Y)
		top := min(points[0].Y, points[2].Y)
		left := min(points[0].X, points[2].X)
		right := max(points[0].X, points[2].X)

		//collision happens, when point is within boundaries
		return (left <= pt.X && pt.X <= right &&
			top <= pt.Y && pt.Y <= bottom), nil
	case PolygonalCollider:
		return PolygonPolygonCollision(pt, obj2), nil
	case VolumetricCollider:
		return obj2.Contains(pt), nil
	default:
		return false, errors.Wrap(NewUnknownShapeError(obj2), "Point collision: ")
	}

}
