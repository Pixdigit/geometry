package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestRect(t *testing.T) {
	r1 := NewRect(Point{25, 25}, 50, 50)
	r2 := NewRect(Point{50, 50}, 50, 50)
	r1.MoveTo(Point{50, 50})

	tools.Test(r1.Equals(r2), "identical rects not percieved as such", t)

	tools.Test(r1.Area() == 2500, "invlid area calculation", t)
	tools.Test(r1.Circumference() == 200, "invlid circumference calculation", t)

	r1.ScaleArea(2)
	tools.Test(abs(r1.Area()-5000) < tolerance, "invlid area calculation after scaling", t)
	tools.Test(abs(r1.Circumference()-282.842712474619) < tolerance, "invlid circumference calculation after scaling", t)
}
