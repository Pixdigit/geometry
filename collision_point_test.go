package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestPointCollision(t *testing.T) {
	pt1 := Point{2, 3}
	pt2 := Point{2, 3}

	collision, err := Collision(pt1, pt2)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "points did not collide when they should", t)

	pt2.MoveTo(Point{1, 2})
	collision, err = Collision(pt1, pt2)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(!collision, "points collided when they should not", t)

	r := NewRect(Point{0, 1}, 2, 2)
	collision, err = Collision(pt2, r)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "point did not collide with rect", t)

	tr := Triangle{Point{0, 0}, Point{5, 0}, Point{0, 5}}
	collision, err = Collision(pt1, tr)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "point did not collide with triangle", t)
}
