package geometry

type Line struct {
	Location
	End Point
}

func NewLine(start, end Point) Line {
	return Line{
		NewLocation(start),
		end,
	}
}

func (l Line) Equals(l2 Line) bool {
	return (l.Ref() == l2.Ref() && l.End == l2.End)
}

func (l Line) Vec() Vec {
	return l.Ref().Distance(l.End)
}

func (l Line) Len() Scalar {
	return l.Vec().Len()
}

func (l *Line) Scale(factor Scalar) {
	vec := l.Ref().Distance(l.End)
	vec.Scale(factor)
	l.End.Add(vec)
}

func (l Line) Points() []Point {
	return []Point{l.Ref(), l.End}
}

func (l Line) Lines() []Line {
	return []Line{l}
}
