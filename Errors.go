package geometry

import "fmt"

type UnkownShapeError struct {
	UnknownShape interface{}
}

func NewUnknownShapeError(unknownShape interface{}) error {
	return UnkownShapeError{
		unknownShape,
	}
}

func (uErr UnkownShapeError) Error() string {
	return fmt.Sprintf("Can not collide with shape of type %T", uErr.UnknownShape)
}
