package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestCircleCollision(t *testing.T) {
	c1 := NewCircle(Point{0, 0}, 1)
	c2 := NewCircle(Point{1, 1}, 1)

	collision, err := Collision(c1, c2)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "circles did not collide when they should", t)

	c2.MoveTo(Point{3, 8})
	collision, err = Collision(c1, c2)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(!collision, "circles collided when they should not", t)

	collision, err = Collision(c1, Point{0.3, 0.3})
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "circle did not contain point as expected", t)

	r := NewRect(Point{-1, 1}, 2, 2)
	collision, err = Collision(c1, r)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "circle did not collide with rect", t)

	tr := Triangle{Point{0, 0}, Point{1, 1}, Point{2, 0}}
	collision, err = Collision(c1, tr)
	if err != nil {
		tools.WrapErr(err, "unable to check for collision", t)
	}
	tools.Test(collision, "circle did not collide with triangle", t)
}
