package geometry

import (
	"math"

	"github.com/pkg/errors"
)

type Circle struct {
	Location
	R Scalar
}

func NewCircle(centerPos Point, r Scalar) Circle {
	return Circle{
		NewLocation(centerPos),
		r,
	}
}

func (c Circle) Area() Scalar {
	return square(c.R) * math.Pi
}

func (c Circle) Circumference() Scalar {
	return 2 * c.R * math.Pi
}

func (c Circle) CollidesWith(geoObj2 interface{}) (bool, error) {
	center := c.Ref()
	switch obj2 := geoObj2.(type) {
	case Circle:
		// Collision when distance between centers is smaller than the
		// sum of the radii
		return center.Distance(obj2.Ref()).LenSqr() <= square(c.R+obj2.R), nil
	case Point:
		return c.Contains(obj2), nil
	case PolygonalCollider:
		return VolumetricPolygonCollision(c, obj2), nil
	default:
		return false, errors.Wrap(NewUnknownShapeError(geoObj2), "Circle: ")
	}
}

func (c Circle) Contains(p PolygonalCollider) bool {
	for _, pt := range p.Points() {
		if c.Ref().Distance(pt).LenSqr() > square(c.R) {
			return false
		}
	}
	return true
}

func (c Circle) Equals(c2 Circle) bool {
	return (c.Location.Ref() == c2.Location.Ref() && c.R == c2.R)
}

func (c Circle) Intersects(p PolygonalCollider) bool {
	switch obj2 := p.(type) {
	case Rect:
		//Check if bounding rect collides

		left := c.ref.X - c.R
		top := c.ref.Y - c.R
		right := c.ref.X + c.R
		bottom := c.ref.Y + c.R

		obj2Top, obj2Left := obj2.ref.X, obj2.ref.Y
		obj2Bottom, obj2Right := obj2Top+obj2.H, obj2Left+obj2.W
		return !(left >= obj2Right ||
			right <= obj2Left ||
			top >= obj2Bottom ||
			bottom <= obj2Top)
	default:
		for _, pt := range p.Points() {
			if c.Contains(pt) {
				return true
			}
		}

		for _, l := range obj2.Lines() {
			//calculate distance from line to center of circle
			//see https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
			start := l.Ref()
			end := l.End
			numerator := abs((end.Y-start.Y)*c.ref.X - (end.X-start.X)*c.ref.Y + end.X*start.Y - end.Y*start.X)
			d := numerator / l.Len()

			if d <= c.R {
				return true
			}
		}
		return false
	}
}

func (c *Circle) Scale(factor Scalar) {
	c.R *= factor
}
func (c *Circle) ScaleArea(factor Scalar) {
	factor = Scalar(math.Sqrt(float64(factor)))
	c.Scale(factor)
}
