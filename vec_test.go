package geometry

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestVec(t *testing.T) {
	v1 := Vec{7, 5}

	v1.Add(Vec{-4, -1})
	tools.Test(v1 == Vec{3, 4}, "invalid addition of vector", t)

	tools.Test(v1.Len() == 5, "invalid distance calculation", t)
	tools.Test(v1.LenSqr() == 25, "invalid distance squared calculation", t)

	v1.Sub(Vec{-4, -1})
	tools.Test(v1 == Vec{7, 5}, "invalid subtraction of vector", t)

	v1.Scale(2)
	tools.Test(v1 == Vec{14, 10}, "invalid scaling of point", t)

	p := v1.ToPoint(Point{0, 0})
	tools.Test(*p == Point{14, 10}, "invalid conversion to point", t)

}
