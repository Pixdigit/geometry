package geometry

import (
	"math"
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestCircleEquality(t *testing.T) {
	c1 := NewCircle(Point{0, -10}, 50)
	c2 := NewCircle(Point{0, 0}, 50)
	c1.MoveRel(Vec{0, 10})
	tools.Test(c1.Equals(c2), "circles do not equal", t)
}

func TestCircleSpecs(t *testing.T) {
	tolerance := Scalar(0.000001)

	c1 := NewCircle(Point{0, 0}, 2)
	tools.Test(c1.Area()-Scalar(4*math.Pi) < tolerance, "circles area incorrect", t)
	tools.Test(c1.Circumference()-Scalar(4*math.Pi) < tolerance, "circles circumference incorrect", t)

	c1.ScaleArea(2)
	tools.Test(c1.Area()-Scalar(8*math.Pi) < tolerance, "circles area incorrect", t)
	tools.Test(c1.Circumference()-Scalar(16*math.Pi) < tolerance, "circles circumference incorrect", t)
}
